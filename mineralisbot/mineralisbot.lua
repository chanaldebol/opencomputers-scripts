--[[
	mineralisbot.lua

	Simple Robot program to automate placement
	and breaking of blocks around an Astral
	Sorcery Metalis ritual

	Requires Inventory and Navigation Upgrades
]]

local math = require("math")
local os = require("os")
local computer = require("computer")
local robot = require("robot")
local component = require("component")

local navigation = component.navigation

local config = {
	maxProjectedCoord = 6, -- distance from pedestal to travel along any axis
	stonePickupX = 6,
	stonePickupZ = -1,
	oreDropoffX = 6,
	oreDropoffZ = 1,
	chargeX = 6,
	chargeZ = 0,
}


function act()
	local pos = navigation.findWaypoints(25)[1]["position"]
	local x = pos[1]
	local z = pos[3]

	-- Maybe pick up stone
	if (x == config["stonePickupX"] and z == config["stonePickupZ"]) then
		robot.turnLeft()
		robot.suck(robot.space())
		robot.turnRight()
	end

	-- Maybe drop off ores
	if (x == config["oreDropoffX"] and z == config["oreDropoffZ"]) then
		robot.turnLeft()
		for slot=2, robot.inventorySize() do
			robot.select(slot)
			robot.drop(robot.count())
		end
		robot.select(1)
		robot.turnRight()
	end

	-- Maybe top off energy
	-- if (x == config["chargeX"] and z == config["chargeZ"]) then
	-- 	while computer.energy() < computer.maxEnergy() - 1 do
	-- 		os.sleep(1)
	-- 	end
	-- end


	-- If not in a corner of the path, handle a block for the ritual
	if (math.abs(x) == config["maxProjectedCoord"] and math.abs(z) ~= config["maxProjectedCoord"])
		or (math.abs(x) ~= config["maxProjectedCoord"] and math.abs(z) == config["maxProjectedCoord"]) then
		robot.turnRight()
		local traversable, subjectBlock = robot.detect()

		-- break blocks that have changed
		if subjectBlock == "air" then
			placeBlock()
		elseif subjectBlock == "solid" and not robot.compare() then
			robot.swing()
			placeBlock()

			-- Stop of the tool is almost broken
			if robot.durability() < 0.1 then
				os.exit()
			end
		end
		robot.turnLeft()
	end

	-- Move to the next position
	if (math.abs(x) == config["maxProjectedCoord"] and math.abs(z) == config["maxProjectedCoord"]) then
		robot.turnRight()
	end
	robot.forward()
end

function placeBlock()
	robot.place()

	-- Stop if we're out of stone
	if robot.count() == 0 then
		robot.turnLeft()
		os.exit()
	end
end

function start()
	robot.select(1)
	while true do
		act()
	end
end

start()
