local shell = require("shell")
local component = require("component")
local event = require("event")
local text = require("text")

local function loadScanners (scannerFile)
	local scannerTable = {}
	for line in io.lines(accessListFile) do
		if line == nil or line == "" then break end
		local record = text.tokenize(line)
		if record ~= nil then
			scannerTable[record[1]] = record[2]
		end
	end

	return scannerTable
end

local function loadAccessList (accessListFile)
	local accessTable = {}
	for line in io.lines(accessListFile) do
		if line == nil or line == "" then break end
		accessTable[line] = true
	end

	return accessTable
end

local args, ops = shell.parse(...)

local scannerTable = {}
if ops.scannerfile == nil then
	scannerTable[component.os_biometric.address] = component.os_doorcontroller.address
else
	print("Loading scanner/door mapping from file " .. ops.scannerfile)
	scannerTable = loadScanners(ops.scannerfile)
end

print("Loading user access from file " .. ops.accesslist)
local accessTable = loadAccessList(ops.accesslist)

while true do
	local event, scanner, bio = event.pullMultiple("bioReader")
	print(event .. " " .. scanner .. " " .. bio)
	if accessTable[bio] then
		component.proxy(scannerTable[scanner]).open()
	end
end
