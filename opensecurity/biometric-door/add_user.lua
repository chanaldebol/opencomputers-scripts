local shell = require("shell")
local component = require("component")
local event = require("event")

local args, ops = shell.parse(...)

local fh = io.open(args[1], "a")
local event, scanner, bio = event.pullMultiple("bioReader")
fh:write(bio .. "\n")
fh:close()
print("Biometric Record added:\n" .. bio .. "\n")
